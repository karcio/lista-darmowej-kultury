# Lista Darmowej Kultury
Lista **legalnej** kultury dostepnej za darmo w internecie. Bez karty debetowej i obowiazku rejestracji. Zbior zawiera glownie tresci dostepne w domenie publicznej.

#Czym jest kultura?

https://pl.wikipedia.org/wiki/Kultura

# Muzyka i koncerty

#Biblioteka Polskiej Piosenki to autoryzowane źródło informacji dotyczących polskiej pieśni i piosenki.
https://bibliotekapiosenki.pl/strona/

#Muzyka klasyczna [EN]
https://www.classical.com/albums

#JazzPRESS to nie tylko jazzowe media, ale przede wszystkim środowisko skupiające pasjonatów jazzu - dziennikarzy, melomanów, organizatorów, promotorów, a czasem także muzyków, mających chęć działania na rzecz jazzu w Polsce
https://jazzpress.pl/

#Ninateka to filmy dokumentalne, fabularne, reportaże, animacje, filmy eksperymentalne, zapisy spektakli teatralnych i operowych, rejestracje koncertów, relacje dokumentujące życie kulturalne i społeczne oraz audycje radiowe.
https://ninateka.pl/

# Elektroniczne wydania ksiazek
Lista zrodel z ktorych mozemy pobrac ksiazki w formacie epub/mobi/pdf.

# Darmowe, ilustrowane książki i komiksy
http://www.lupuslibri.pl/p/ksiazki-i-komiksy-online-za-darmo.html

#Antologie fantastyki
https://fantazmaty.pl/czytaj/antologie/nanofantazje-1-0/

#FantastykaPolska.pl
http://www.bazaebokow.robertjszmidt.pl/

#Portal Instytutu Pamięci Narodowej
http://pamiec.pl/

#Projekt za ktorym stoi fundacja Festina Lente
http://www.chmuraczytania.pl/

### Darmowe ksiazki od wydawnictw - Wobilink, Publio, Virtualo, Nexto, eBookPoint, Legimi, czy eplaton.pl 

#Sekcja z darmowymi ksiazkami od wydawnictwa publio
http://www.publio.pl/e-booki,darmowe.html

#Sekcja z darmowymi ksiazkami od wydawnictwa virtualo
https://virtualo.pl/darmowe/m6/

#Darmowe lektury
https://wolnelektury.pl/

#Darmowe klasyki literatury
http://bookini.pl/

#Narodowe Centrum Kultury 
https://www.nck.pl/

#Wikizrodla wolna biblioteka
https://pl.wikisource.org/wiki/Wiki%C5%BAr%C3%B3d%C5%82a:Strona_g%C5%82%C3%B3wna

#Polona to stworzony przez Bibliotekę Narodową portal, za pośrednictwem którego zbiory Biblioteki Narodowej i innych instytucji są udostępniane bezpłatnie w postaci cyfrowej. 
https://polona.pl/

https://openlibrary.org/

http://www.classicreader.com/

https://comicbookplus.com/

# Audiobooki

#Audioteka
https://www.youtube.com/user/audiotekapl/videos

#Sluchaj za darmo
https://www.youtube.com/user/SluchajZaDarmo

#Audiobooki po angielsku
http://www.audiobooktreasury.com/

# Film

### Kanaly YouTube

#Sala Kinowa to kanał filmowy z ponad 170 pełnometrażowymi produkcjami z całego świata. Filmy są dostępne z polskimi napisami bądź lektorem
https://www.youtube.com/user/SalaKinowa/videos

#Studio Filmowe TOR - oficjalny kanal
https://www.youtube.com/user/StudioFilmoweTOR/videos

#Oficjalny kanał Studia Filmowego KRONIKA „Polska Kronika Filmowa" (PKF) 
https://www.youtube.com/user/PolskaKronikaFilmowa

#Oficjalny kanal YouTube Studio Miniatur Filmowych
https://www.youtube.com/user/StudioMiniaturF

#animacje wyprodukowane lub wyselekcjonowane przez studio Se-ma-for.
https://www.youtube.com/user/semafortv/about

#Kino Rosyjskie
http://cinema.mosfilm.ru/
https://www.youtube.com/user/mosfilm

#Wajda School and Studio to wyjątkowy kanał prezentujący osiągnięcia młodych filmowców.
https://www.youtube.com/user/wajdaschoolandstudio

#Archiwum Szkoły Filmowej w Łodzi zawiera unikatowy zbiór ponad pięciu tysięcy etiud filmowych realizowanych od roku 1949 przez studentów Szkoły Filmowej.
https://etiudy.filmschool.lodz.pl/
https://etiudy.filmschool.lodz.pl/catalog?page=1

#ARTE rozpowszechnia wszystkie gatunki utworów audiowizualnych. Około 55% stanowią dokumenty, 25% filmy i seriale telewizyjne, 15% programy informacyjne, a 5% muzyka i występy artystyczne. Dwie trzecie programów emitowanych w ARTE jest prezentowanych na antenie po raz pierwszy.
https://www.arte.tv/pl/videos/080614-007-A/the-rise-of-graffiti-writing-2/

#Portal filmowy dla niewidomych i niesłyszących
https://adapter.pl/

#W ramach projektu Dokument Cyfrowo, digitalizowane są najcenniejsze dzieła polskiej szkoły dokumentu
http://www.dokumentcyfrowo.pl/

# Edukacja - kursy i szkolenia

#Kurs Linux

https://miroslawzelent.pl/kurs-linux/

# Gry komputerowe

#Open Source Game Clones
https://osgameclones.com/

#0 A.D
https://play0ad.com/

#FreeCiv
http://www.freeciv.org/

#The Dark MOD
http://www.thedarkmod.com/main/

#Freedoom
https://freedoom.github.io/

#OpenTTD
https://www.openttd.org/

#Diablo Roguelike
https://diablo.chaosforge.org/downloads

#Zaawansowany symulator lotniczy kompatybilny z Windows i Mac.
https://www.flightgear.org/

## Angielskie zrodla

#Internet Archive is a non-profit library of millions of free books, movies, software, music, websites, and more. 
https://archive.org/

#Standard Ebooks is a volunteer driven, not-for-profit project that produces new editions of public domain ebooks that are lovingly formatted, open source, and free
https://standardebooks.org/

## Darmowe mapy i nawigacja

#OpenStreetMap
https://www.openstreetmap.org/

#Darmowa nawigacja na Android/ios dziala offline.
https://osmand.net/

## Alternatywne oprogramowanie dla routera

#OpenWRT
https://openwrt.org/

## Oprogramowanie

#Alternatywny sklep dla Android
https://f-droid.org/en/

#Darmmowy certyfikat SSL
https://letsencrypt.org/

#102 darmowe fonty z polskimi znakami
https://getfound.pl/102-darmowe-fonty-z-polskimi-znakami/

## Nauka Angielskiego

#Przydatne zwroty i powiedzonka
https://www.phrases.org.uk/meanings/phrases-and-sayings-list.html
